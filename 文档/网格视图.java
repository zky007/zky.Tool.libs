package a.aa.aaa;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;

public class 网格视图 extends View {
    private Paint 网格画笔 = new Paint();
    private int 网格数 = 10; // 网格的行数和列数
    private float 缩放因子 = 1.0f;
    private float 最后触摸X;
    private float 最后触摸Y;
    private float 滚动X = 0;
    private float 滚动Y = 0;
    private ScaleGestureDetector 缩放检测器;

    public 网格视图(Context context, AttributeSet attrs) {
        super(context, attrs);
        网格画笔.setColor(0xFF000000); // 黑色网格
        网格画笔.setStrokeWidth(2);
        缩放检测器 = new ScaleGestureDetector(context, new ScaleListener());
        android.util.Log.i("TieApp","测试");
    }

    @Override
    protected void onDraw(Canvas 画布) {
        super.onDraw(画布);
        final int 宽度 = getWidth();
        final int 高度 = getHeight();
        final int 单元格大小 = (int) (Math.min(宽度, 高度) / 网格数 * 缩放因子);

        画布.save();
        画布.translate(滚动X, 滚动Y);

        for (int i = 0; i <= 网格数; i++) {
            画布.drawLine(i * 单元格大小, 0, i * 单元格大小, 高度, 网格画笔);
            画布.drawLine(0, i * 单元格大小, 宽度, i * 单元格大小, 网格画笔);
        }

        画布.restore();
    }

    @Override
    public boolean onTouchEvent(MotionEvent 事件) {
        缩放检测器.onTouchEvent(事件);

        switch (事件.getAction()) {
            case MotionEvent.ACTION_DOWN:
                最后触摸X = 事件.getX();
                最后触摸Y = 事件.getY();
                计算索引(事件.getX(), 事件.getY());
                return true;
            case MotionEvent.ACTION_MOVE:
                if (!缩放检测器.isInProgress()) {
                    float dx = 事件.getX() - 最后触摸X;
                    float dy = 事件.getY() - 最后触摸Y;
                    滚动X += dx;
                    滚动Y += dy;
                    invalidate();
                }
                最后触摸X = 事件.getX();
                最后触摸Y = 事件.getY();
                return true;
        }
        return super.onTouchEvent(事件);
    }

    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            缩放因子 *= detector.getScaleFactor();
            缩放因子 = Math.max(0.1f, Math.min(缩放因子, 5.0f));
            invalidate();
            return true;
        }
    }

    private void 计算索引(float x, float y) {
        int 单元格大小 = (int) (Math.min(getWidth(), getHeight()) / 网格数 * 缩放因子);
        int 索引X = (int) ((x - 滚动X) / 单元格大小);
        int 索引Y = (int) ((y - 滚动Y) / 单元格大小);
        if (索引X >= 0 && 索引X < 网格数 && 索引Y >= 0 && 索引Y < 网格数) {
            触发单元格点击(索引X, 索引Y);
        }
    }

    private void 触发单元格点击(int 索引X, int 索引Y) {
        // 这里可以添加点击单元格后的逻辑，例如更新界面、触发事件等
        // 例如打印点击的单元格索引
        android.util.Log.i("TieApp","点击了单元格: (" + 索引X + ", " + 索引Y + ")");
        System.out.println("点击了单元格: (" + 索引X + ", " + 索引Y + ")");
        // 实际应用中可能会调用一个回调函数或者发送一个事件通知
    }
}