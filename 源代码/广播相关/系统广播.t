@导入Java("android.content.*")
类 屏幕方向广播 : 广播接收器
	变量 方向 : 整数 = 1
	事件 屏幕方向广播:接收到广播(环境 : 安卓环境,数据 : 启动信息)
		提示框.弹出提示(环境,方向+")")
		如果 方向 == 0 则
			方向 = 1
			否则
				方向 = 0
		结束 如果
		方向被改变()
	结束 事件
	//使用这个方法动态注册广播，获取到屏幕方向广播对象监听对象既可
	@静态
	方法 注册广播(窗口对象 : 安卓窗口) : 屏幕方向广播
		
		@code
			#<屏幕方向广播> broadcastReceive = new #<屏幕方向广播>();
			//注册广播接收,注意：要监听这个系统广播就必须用这种方式来注册，不能再xml中注册，否则不能生效
			IntentFilter filter = new IntentFilter();
			filter.addAction("android.intent.action.CONFIGURATION_CHANGED");
			#窗口对象.registerReceiver(broadcastReceive,filter);
			broadcastReceive.#方向 = #窗口对象.getResources().getConfiguration().orientation;
			return broadcastReceive;
@end
	结束 方法
	
	//当屏幕方向被改变时会调用该方法
	定义事件 方向被改变()
	
结束 类

@导入Java("android.net.*")
@导入Java("android.content.*")
@导入Java("android.telephony.TelephonyManager")
@附加权限("android.permission.ACCESS_NETWORK_STATE")
类 网络状态广播 : 广播接收器
	@静态
	常量 类型_WIFI : 整数 = 0
	@静态
	常量 类型_2G : 整数 = 1
	@静态
	常量 类型_3G : 整数 = 2
	@静态
	常量 类型_4G : 整数 = 3
	@静态
	常量 类型_5G : 整数 = 4
	@静态
	常量 类型_未知 : 整数 = -2
	@静态
	常量 类型_无网络 : 整数 = -1
	
	事件 网络状态广播:接收到广播(环境 : 安卓环境,数据 : 启动信息)
		@code
		// 网络状态发生变化时的处理逻辑
        if (ConnectivityManager.CONNECTIVITY_ACTION.equals(#数据.getAction())) {
            ConnectivityManager connectivityManager = (ConnectivityManager) #环境.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                // 网络已连接
                String networkType = networkInfo.getTypeName();
				if (networkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
				//网络为WIFI
					#网络发生变化(true,#类型_WIFI);
					} else if (networkInfo.getType() == ConnectivityManager.TYPE_MOBILE) {
				//移动网络
				 // 当前是移动网络（可能是3G、4G等）
                    switch (networkInfo.getSubtype()) {
                        case TelephonyManager.NETWORK_TYPE_GPRS:
                        case TelephonyManager.NETWORK_TYPE_EDGE:
                        case TelephonyManager.NETWORK_TYPE_CDMA:
                        case TelephonyManager.NETWORK_TYPE_1xRTT:
                        case TelephonyManager.NETWORK_TYPE_IDEN:
                            // 2G网络
							#网络发生变化(true,#类型_2G);
                            break;
                        case TelephonyManager.NETWORK_TYPE_UMTS:
                        case TelephonyManager.NETWORK_TYPE_EVDO_0:
                        case TelephonyManager.NETWORK_TYPE_EVDO_A:
                        case TelephonyManager.NETWORK_TYPE_HSDPA:
                        case TelephonyManager.NETWORK_TYPE_HSUPA:
                        case TelephonyManager.NETWORK_TYPE_HSPA:
                        case TelephonyManager.NETWORK_TYPE_EVDO_B:
                        case TelephonyManager.NETWORK_TYPE_EHRPD:
                        case TelephonyManager.NETWORK_TYPE_HSPAP:
                            // 3G网络
							#网络发生变化(true,#类型_3G);
                            break;
                        case TelephonyManager.NETWORK_TYPE_LTE:
                            // 4G网络
							#网络发生变化(true,#类型_4G);
                            break;
							case TelephonyManager.NETWORK_TYPE_NR:
							#网络发生变化(true,#类型_5G);
                        default:
                            // 未知网络类型
							#网络发生变化(true,#类型_未知);
                            break;
                    }
				}
                // 处理网络连接的逻辑
            } else {
                // 网络已断开
                // 处理网络断开的逻辑
				#网络发生变化(false,#类型_无网络);
            }
			}
			
			/*
			public class NetworkChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (ConnectivityManager.CONNECTIVITY_ACTION.equals(intent.getAction())) {
            NetworkInfo networkInfo = (NetworkInfo) intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
            if (networkInfo != null && networkInfo.isConnected()) {
                if (networkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                    // 当前是WiFi网络
                } else if (networkInfo.getType() == ConnectivityManager.TYPE_MOBILE) {
                   
                }
            } else {
                // 没有网络连接
            }
        }
    }
}
*/
			@end
	结束 事件
	
	//使用这个方法注册广播
	@静态
	方法 注册广播(窗口对象 : 安卓窗口) : 网络状态广播
		变量 网络状态 : 网络状态广播
		@code
		IntentFilter filter = new IntentFilter();
			filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
			#窗口对象.registerReceiver(#网络状态,filter);
			return #网络状态;
			@end
			
	结束 方法	
	
	//当网络发生变化时调用
	定义事件 网络发生变化(是否连网 : 逻辑型,网络类型 : 整数)
结束 类