@强制输出
@禁止创建对象
@指代类("android.animation.Animator")
类 安卓动画
	@静态
	常量 持续时间_无限 : 长整数 = -1
		
	方法 添加监听程序(监听器对象 : 安卓动画_监听器) 
		code #this.addListener((android.animation.Animator.AnimatorListener) #监听器对象);
	结束 方法
		
	方法 添加暂停监听程序(监听器对象 : 安卓动画_暂停播放监听器) 
		code #this.addPauseListener((android.animation.Animator.AnimatorPauseListener) #监听器对象);
	结束 方法
		
	方法 取消() 
		code #this.cancel();
	结束 方法
		
	方法 克隆()  : 安卓动画
		code return #this.clone();
	结束 方法
		
	方法 克隆()  : 对象
		code return #this.clone();
	结束 方法
		
	方法 结束播放() 
		code #this.end();
	结束 方法
		
	属性读 持续时间() : 长整数
		code return #this.getDuration();
	结束 属性
		
	属性读 时间插值器() : 安卓动画_时间插值器
		code return #this.getInterpolator();
	结束 属性
		
	属性读 启动延迟时间() : 长整数
		code return #this.getStartDelay();
	结束 属性
		
	属性读 总持续时间() : 长整数
		code return #this.getTotalDuration();
	结束 属性
		
	方法 已暂停()  : 逻辑型
		code return #this.isPaused();
	结束 方法
		
	方法 正在运行()  : 逻辑型
		code return #this.isRunning();
	结束 方法
		
	方法 已启动()  : 逻辑型
		code return #this.isStarted();
	结束 方法
		
	方法 暂停动画() 
		code #this.pause();
	结束 方法
		
	方法 删除所有监听器() 
		code #this.removeAllListeners();
	结束 方法
		
	方法 删除监听程序(监听器对象 : 安卓动画_监听器) 
		code #this.removeListener((android.animation.Animator.AnimatorListener) #监听器对象);
	结束 方法
		
	方法 删除暂停监听程序(监听器对象 : 安卓动画_暂停播放监听器) 
		code #this.removePauseListener((android.animation.Animator.AnimatorPauseListener) #监听器对象);
	结束 方法
		
	方法 继续播放() 
		code #this.resume();
	结束 方法
		
	方法 设置持续时间(持续时间 : 长整数)  : 安卓动画
		code return #this.setDuration(#持续时间);
	结束 方法
		
	属性写 设置时间插值器(时间插值器 : 安卓动画_时间插值器) 
		code #this.setInterpolator(#时间插值器);
	结束 属性
		
	属性写 设置启动延迟(延长时间 : 长整数) 
		code #this.setStartDelay(#延长时间);
	结束 属性
		
	属性写 设置目标(arg0 : 对象) 
		code #this.setTarget(#arg0);
	结束 属性
		
	方法 设置结束值() 
		code #this.setupEndValues();
	结束 方法
		
	方法 设置起始值() 
		code #this.setupStartValues();
	结束 方法
		
	方法 开始播放() 
		code #this.start();
	结束 方法

结束 类