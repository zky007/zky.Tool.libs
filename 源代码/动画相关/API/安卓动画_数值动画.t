@指代类("android.animation.ValueAnimator")
类 安卓动画_数值动画 : 安卓动画

	@静态
	常量 模式_无限 : 整数 = -1
	@静态
	常量 模式_重启 : 整数 = 1
	@静态
	常量 模式_反向 : 整数 = 2
		
@静态
	属性读 动画已启用() : 逻辑型
		code return #<@安卓动画_数值动画>.areAnimatorsEnabled();
	结束 属性
		

@静态
	方法 持续时间_单精度()  : 单精度小数
		code return #<@安卓动画_数值动画>.getDurationScale();
	结束 方法
		
@静态
	属性读 帧延迟() : 长整数
		code return #<@安卓动画_数值动画>.getFrameDelay();
	结束 属性
		

@静态
	方法 过渡_颜色(颜色值 : 整数[])  : 安卓动画_数值动画
		code return #<@安卓动画_数值动画>.ofArgb(#颜色值);
	结束 方法
		

@静态
	方法 过渡_单精度小数(单精度小数组 : 单精度小数[])  : 安卓动画_数值动画
		code return #<@安卓动画_数值动画>.ofFloat(#单精度小数组);
	结束 方法
		

@静态
	方法 过渡_整数(整数组 : 整数[])  : 安卓动画_数值动画
		code return #<@安卓动画_数值动画>.ofInt(#整数组);
	结束 方法
		

@静态
	方法 过渡_对象(估值器 : 安卓动画_估值器 ,对象组 : 对象[])  : 安卓动画_数值动画
		code return #<@安卓动画_数值动画>.ofObject(#估值器 ,#对象组);
	结束 方法
		

@静态
	方法 物业价值百分比持有者(arg0 : 安卓动画_属性值[])  : 安卓动画_数值动画
		code return #<@安卓动画_数值动画>.ofPropertyValuesHolder(#arg0);
	结束 方法
		
@静态
	属性写 持续时间_单精度(持续时间 : 单精度小数) 
		code #<@安卓动画_数值动画>.setDurationScale(#持续时间);
	结束 属性
		
@静态
	属性写 帧延迟(每一帧延迟时间 : 长整数) 
		code #<@安卓动画_数值动画>.setFrameDelay(#每一帧延迟时间);
	结束 属性
		
	方法 添加更新监听程序(监听器对象 : 安卓动画_更新监听器) 
		code #this.addUpdateListener((android.animation.ValueAnimator.AnimatorUpdateListener) #监听器对象);
	结束 方法
		
	方法 取消() 
		code #this.cancel();
	结束 方法
		
	方法 克隆()  : 安卓动画_数值动画
		code return #this.clone();
	结束 方法
		
	方法 结束动画() 
		code #this.end();
	结束 方法
		
	属性读 动画分数() : 单精度小数
		code return #this.getAnimatedFraction();
	结束 属性
	
	//当前过渡到的值	
	属性读 动画值() : 对象
		code return #this.getAnimatedValue();
	结束 属性
		
	方法 获取动画值(arg0 : 文本)  : 对象
		code return #this.getAnimatedValue(#arg0);
	结束 方法
	
	//获取当前播放的位置	
	属性读 当前播放时间() : 长整数
		code return #this.getCurrentPlayTime();
	结束 属性
	
	//获取该动画可持续的时间	
	属性读 持续时间() : 长整数
		code return #this.getDuration();
	结束 属性
		
	方法 获取插补器()  : 安卓动画_时间插值器
		code return #this.getInterpolator();
	结束 方法
	
	//获取该动画重复的次数	
	属性读 重复次数() : 整数
		code return #this.getRepeatCount();
	结束 属性
	
	//获取该动画的重复模式，具体可看，该类的静态常量	
	属性读 重复模式() : 整数
		code return #this.getRepeatMode();
	结束 属性
	
	//获取播放开始时的延迟时间	
	属性读 启动延迟() : 长整数
		code return #this.getStartDelay();
	结束 属性
	
	//获取该动画的应当播放的时间	
	属性读 总播放时间() : 长整数
		code return #this.getTotalDuration();
	结束 属性
		
	方法 获取值()  : 安卓动画_属性值[]
		code return #this.getValues();
	结束 方法
		
	属性读 正在运行() : 逻辑型
		code return #this.isRunning();
	结束 属性
		
	属性读 已启动() : 逻辑型
		code return #this.isStarted();
	结束 属性
		
	方法 暂停() 
		code #this.pause();
	结束 方法
		
	方法 删除所有更新监听程序() 
		code #this.removeAllUpdateListeners();
	结束 方法
		
	属性写 删除更新监听程序(监听器 : 对象) 
		code #this.removeUpdateListener(#监听器);
	结束 属性
		
	方法 继续播放() 
		code #this.resume();
	结束 方法
		
	方法 反转() 
		code #this.reverse();
	结束 方法
		
	属性写 当前分数(分数 : 单精度小数) 
		code #this.setCurrentFraction(#分数);
	结束 属性
	
	//直接设置当前播放的时间位置	
	属性写 当前播放时间(播放时间 : 长整数) 
		code #this.setCurrentPlayTime(#播放时间);
	结束 属性
		
	方法 置持续时间(持续时间 : 长整数)  : 安卓动画_数值动画
		code return #this.setDuration(#持续时间);
	结束 方法
		
	方法 设置赋值器(arg0 : 安卓动画_估值器) 
		code #this.setEvaluator(#arg0);
	结束 方法
		
	方法 设置浮点值(arg0 : 单精度小数[]) 
		code #this.setFloatValues(#arg0);
	结束 方法
		
	方法 设置整数值(arg0 : 整数[]) 
		code #this.setIntValues(#arg0);
	结束 方法
		
	方法 设置插补器(arg0 : 安卓动画_时间插值器) 
		code #this.setInterpolator(#arg0);
	结束 方法
		
	方法 设置对象值(arg0 : 对象[]) 
		code #this.setObjectValues(#arg0);
	结束 方法
		
	属性写 重复次数(次数 : 整数) 
		code #this.setRepeatCount(#次数);
	结束 属性
		
	属性写 重复模式(模式 : 整数) 
		code #this.setRepeatMode(#模式);
	结束 属性
		
	属性写 启动延迟(延迟时间 : 长整数) 
		code #this.setStartDelay(#延迟时间);
	结束 属性
		
	方法 设置值(arg0 : 安卓动画_属性值[]) 
		code #this.setValues(#arg0);
	结束 方法
		
	方法 开始播放() 
		code #this.start();
	结束 方法
	
	方法 倒放() 
		code #this.reverse();
	结束 方法

结束 类