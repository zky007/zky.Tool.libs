@强制输出
@指代类("android.animation.PropertyValuesHolder")
类 安卓动画_属性值
		

@静态
	方法 浮点数(arg0 : 安卓_工具_属性 ,arg1 : 单精度小数[])  : 安卓动画_属性值
		code return #<@安卓动画_属性值>.ofFloat(#arg0 ,#arg1);
	结束 方法
		

@静态
	方法 浮点数(arg0 : 文本 ,arg1 : 单精度小数[])  : 安卓动画_属性值
		code return #<@安卓动画_属性值>.ofFloat(#arg0 ,#arg1);
	结束 方法
		

@静态
	方法 来自Int(arg0 : 安卓_工具_属性 ,arg1 : 整数[])  : 安卓动画_属性值
		code return #<@安卓动画_属性值>.ofInt(#arg0 ,#arg1);
	结束 方法
		

@静态
	方法 来自Int(arg0 : 文本 ,arg1 : 整数[])  : 安卓动画_属性值
		code return #<@安卓动画_属性值>.ofInt(#arg0 ,#arg1);
	结束 方法
		

@静态
	方法 关键帧的数量(arg0 : 安卓_工具_属性 ,arg1 : 安卓动画_关键帧[])  : 安卓动画_属性值
		code return #<@安卓动画_属性值>.ofKeyframe(#arg0 ,#arg1);
	结束 方法
		

@静态
	方法 关键帧的数量(arg0 : 文本 ,arg1 : 安卓动画_关键帧[])  : 安卓动画_属性值
		code return #<@安卓动画_属性值>.ofKeyframe(#arg0 ,#arg1);
	结束 方法
		

@静态
	方法 多个浮点数(arg0 : 文本 ,arg1 : 安卓动画_类型转换器 ,arg2 : 安卓动画_估值器 ,arg3 : 对象[])  : 安卓动画_属性值
		code return #<@安卓动画_属性值>.ofMultiFloat(#arg0 ,#arg1 ,#arg2 ,#arg3);
	结束 方法
		

@静态
	方法 多个浮点数(arg0 : 文本 ,arg1 : 对象)  : 安卓动画_属性值
		code return #<@安卓动画_属性值>.ofMultiFloat(#arg0 ,(android.graphics.Path) #arg1);
	结束 方法
		

@静态
	方法 多个浮点数(arg0 : 文本 ,arg1 : 单精度小数[][])  : 安卓动画_属性值
		code return #<@安卓动画_属性值>.ofMultiFloat(#arg0 ,#arg1);
	结束 方法
		

@静态
	方法 多个国际的(arg0 : 文本 ,arg1 : 安卓动画_类型转换器 ,arg2 : 安卓动画_估值器 ,arg3 : 对象[])  : 安卓动画_属性值
		code return #<@安卓动画_属性值>.ofMultiInt(#arg0 ,#arg1 ,#arg2 ,#arg3);
	结束 方法
		

@静态
	方法 多个国际的(arg0 : 文本 ,arg1 : 对象)  : 安卓动画_属性值
		code return #<@安卓动画_属性值>.ofMultiInt(#arg0 ,(android.graphics.Path) #arg1);
	结束 方法
		

@静态
	方法 多个国际的(arg0 : 文本 ,arg1 : 整数[][])  : 安卓动画_属性值
		code return #<@安卓动画_属性值>.ofMultiInt(#arg0 ,#arg1);
	结束 方法
		

@静态
	方法 对象的数量(arg0 : 安卓_工具_属性 ,arg1 : 安卓动画_类型转换器 ,arg2 : 安卓动画_估值器 ,arg3 : 对象[])  : 安卓动画_属性值
		code return #<@安卓动画_属性值>.ofObject(#arg0 ,#arg1 ,#arg2 ,#arg3);
	结束 方法
		

@静态
	方法 对象的数量(arg0 : 安卓_工具_属性 ,arg1 : 安卓动画_类型转换器 ,arg2 : 对象)  : 安卓动画_属性值
		code return #<@安卓动画_属性值>.ofObject(#arg0 ,#arg1 ,(android.graphics.Path) #arg2);
	结束 方法
		

@静态
	方法 对象的数量(arg0 : 安卓_工具_属性 ,arg1 : 安卓动画_估值器 ,arg2 : 对象[])  : 安卓动画_属性值
		code return #<@安卓动画_属性值>.ofObject(#arg0 ,#arg1 ,#arg2);
	结束 方法
		

@静态
	方法 对象的数量(arg0 : 文本 ,arg1 : 安卓动画_类型转换器 ,arg2 : 对象)  : 安卓动画_属性值
		code return #<@安卓动画_属性值>.ofObject(#arg0 ,#arg1 ,(android.graphics.Path) #arg2);
	结束 方法
		

@静态
	方法 对象的数量(arg0 : 文本 ,arg1 : 安卓动画_估值器 ,arg2 : 对象[])  : 安卓动画_属性值
		code return #<@安卓动画_属性值>.ofObject(#arg0 ,#arg1 ,#arg2);
	结束 方法
		
	方法 克隆()  : 安卓动画_属性值
		code return #this.clone();
	结束 方法
		
	方法 克隆()  : 对象
		code return #this.clone();
	结束 方法
		
	方法 获取属性名称()  : 文本
		code return #this.getPropertyName();
	结束 方法
		
	方法 SET转换器(arg0 : 安卓动画_类型转换器) 
		code #this.setConverter(#arg0);
	结束 方法
		
	方法 设置赋值器(arg0 : 安卓动画_估值器) 
		code #this.setEvaluator(#arg0);
	结束 方法
		
	方法 设置浮点值(arg0 : 单精度小数[]) 
		code #this.setFloatValues(#arg0);
	结束 方法
		
	方法 设置Int值(arg0 : 整数[]) 
		code #this.setIntValues(#arg0);
	结束 方法
		
	方法 设置关键帧(arg0 : 安卓动画_关键帧[]) 
		code #this.setKeyframes(#arg0);
	结束 方法
		
	方法 设置对象值(arg0 : 对象[]) 
		code #this.setObjectValues(#arg0);
	结束 方法
		
	方法 设置属性(arg0 : 安卓_工具_属性) 
		code #this.setProperty(#arg0);
	结束 方法
		
	方法 设置属性名称(arg0 : 文本) 
		code #this.setPropertyName(#arg0);
	结束 方法
		
	方法 要串起来()  : 文本
		code return #this.toString();
	结束 方法

结束 类
