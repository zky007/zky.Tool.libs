
@后缀代码("implements android.animation.Animator.AnimatorPauseListener")
类 安卓动画_暂停播放监听器
	
	//当动画暂停播放时，调用该事件
	@强制输出
	@输出名("onAnimationPause")
	定义事件 动画暂停播放(动画对象 : 安卓动画)	
	//当动画继续播放时，调用该事件
	@强制输出
	@输出名("onAnimationResume")
	定义事件 动画继续播放(动画对象 : 安卓动画)
	
结束 类