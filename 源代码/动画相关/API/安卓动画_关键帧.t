@禁止创建对象
@指代类("android.animation.Keyframe")
类 安卓动画_关键帧
		

@静态
	方法 浮点数(arg0 : 单精度小数)  : 安卓动画_关键帧
		code return #<@安卓动画_关键帧>.ofFloat(#arg0);
	结束 方法
		

@静态
	方法 浮点数(arg0 : 单精度小数 ,arg1 : 单精度小数)  : 安卓动画_关键帧
		code return #<@安卓动画_关键帧>.ofFloat(#arg0 ,#arg1);
	结束 方法
		

@静态
	方法 来自Int(arg0 : 单精度小数)  : 安卓动画_关键帧
		code return #<@安卓动画_关键帧>.ofInt(#arg0);
	结束 方法
		

@静态
	方法 来自Int(arg0 : 单精度小数 ,arg1 : 整数)  : 安卓动画_关键帧
		code return #<@安卓动画_关键帧>.ofInt(#arg0 ,#arg1);
	结束 方法
		

@静态
	方法 对象的数量(arg0 : 单精度小数)  : 安卓动画_关键帧
		code return #<@安卓动画_关键帧>.ofObject(#arg0);
	结束 方法
		

@静态
	方法 对象的数量(arg0 : 单精度小数 ,arg1 : 对象)  : 安卓动画_关键帧
		code return #<@安卓动画_关键帧>.ofObject(#arg0 ,#arg1);
	结束 方法
		
	方法 克隆()  : 对象
		code return #this.clone();
	结束 方法
		
	方法 获取分数()  : 单精度小数
		code return #this.getFraction();
	结束 方法
		
	方法 获取插补器()  : 安卓动画_时间插值器
		code return #this.getInterpolator();
	结束 方法
		
	方法 获取类型()  : Java类
		code return #this.getType();
	结束 方法
		
	方法 有价值()  : 逻辑型
		code return #this.hasValue();
	结束 方法
		
	方法 集合分数(arg0 : 单精度小数) 
		code #this.setFraction(#arg0);
	结束 方法
		
	方法 设置插补器(arg0 : 安卓动画_时间插值器) 
		code #this.setInterpolator(#arg0);
	结束 方法

结束 类

类 安卓动画_关键帧_实现 : 安卓动画_关键帧

		
@虚拟方法
	@输出名("clone")
	方法 克隆()  : 安卓动画_关键帧
		code return super.clone();
	结束 方法

		
@虚拟方法
	@输出名("getValue")
	方法 获取价值()  : 对象
		code return super.getValue();
	结束 方法

		
@虚拟方法
	@输出名("setValue")
	方法 设定值(arg0 : 对象) 
		code super.setValue(#arg0);
	结束 方法

结束 类