包名 安卓.数据库
@导入Java("android.database.sqlite.SQLiteDatabase")
@导入Java("java.io.*")
@导入Java("android.database.Cursor")
@导入Java("android.content.ContentValues")
@禁止创建对象
@指代类("android.database.sqlite.SQLiteDatabase")
类 数据库管理

	//（CONFLICT_ABORT）当发生约束违规时，不会执行 ROLLBACK，因此保留同一事务中先前命令的更改。这是默认行为。
	@静态
	常量 冲突_中止 为 整数 = 2
	//（CONFLICT_FAIL）当发生约束冲突时，命令中止并返回代码 SQLITE_CONSTRAINT。但是该命令在遇到约束违规之前对数据库所做的任何更改都将保留并且不会被取消。
	@静态
	常量 冲突_失败 为 整数 = 3
	//（CONFLICT_IGNORE）发生约束冲突时，不插入或更改包含约束冲突的行。但该命令继续正常执行。包含违反约束的行之前和之后的其他行继续正常插入或更新。不返回错误。
	@静态
	常量 冲突_忽略 为 整数 = 4
	//（CONFLICT_NONE）未指定冲突操作时，请使用以下内容。
	@静态
	常量 冲突_无 为 整数 = 0
	//（CONFLICT_REPLACE）当发生 UNIQUE 约束冲突时，会在插入或更新当前行之前删除导致约束冲突的预先存在的行。因此插入或更新总是发生。命令继续正常执行。不返回错误。如果发生 NOT NULL 约束冲突，NULL 值将替换为该字段的默认值。如果字段没有默认值，则使用 ABORT 算法。如果发生 CHECK 约束违规，则使用 IGNORE 算法。当此冲突解决策略删除行以满足约束时，它不会在这些行上调用删除触发器。此行为可能会在未来版本中更改。
	@静态
	常量 冲突_替换 为 整数 = 5
	//（CONFLICT_ROLLBACK）当约束违反发生时，立即发生 ROLLBACK，从而结束当前事务，并且命令中止并返回代码 SQLITE_CONSTRAINT。如果没有事务处于活动状态（除了在每个命令上创建的隐含事务），则该算法的工作方式与 ABORT 相同。
	@静态
	常量 冲突_回滚 为 整数 = 1
	//（CREATE_IF_NECESSARY）如果数据库文件不存在，则用于创建数据库文件的标志。
	@静态
	常量 打开标志_自动创建 为 整数 = 268435456
	//（ENABLE_WRITE_AHEAD_LOGGING）打开数据库文件的标志，默认情况下启用预写日志记录。使用此标志比调用｛enableWriteAheadLogging()｝. 预写日志不能用于只读数据库，因此如果数据库以只读方式打开，则忽略此标志的值。
	@静态
	常量 打开标志_预写日志记录 为 整数 = 536870912
	//（OPEN_READONLY）以只读方式打开数据库的标志。如果磁盘可能已满，这是打开数据库的唯一可靠方法。
	@静态
	常量 打开标志_只读 为 整数 = 1
	//（OPEN_READWRITE）打开数据库进行读写的标志。如果磁盘已满，甚至在您实际写入任何内容之前这可能会失败。
	@静态
	常量 打开标志_读写 为 整数 = 0
	//（NO_LOCALIZED_COLLATORS）在不支持本地化整理器的情况下打开数据库的标志。这会导致LOCALIZED不创建整理器。使用此标志以使用创建数据库时使用的设置时，您必须保持一致。如果设置了这个，｛setLocale(Locale)｝则什么都不做。
	@静态
	常量 打开标志_没有本地化的拼贴器 为 整数 = 16
	//（MAX_SQL_CACHE_SIZE）可以设置的绝对最大值｛setMaxSqlCacheSize(int)｝。每个准备好的语句在 1K - 6K 之间，具体取决于 SQL 语句和模式的复杂性。大型 SQL 缓存可能会使用大量内存。
	@静态
	常量 最大SQL缓存大小 为 整数 = 100
	//（SQLITE_MAX_LIKE_PATTERN_LENGTH）LIKE 或 GLOB 模式的最大长度 SQLite 的默认 LIKE 和 GLOB 实现中使用的模式匹配算法在某些病理情况下可以表现出 O(N^2) 性能（其中 N 是模式中的字符数）。为了避免拒绝服务攻击，LIKE 或 GLOB 模式的长度限制为 SQLITE_MAX_LIKE_PATTERN_LENGTH 字节。此限制的默认值为 50000。现代工作站甚至可以相对快速地评估 50000 字节的病理 LIKE 或 GLOB 模式。只有当模式长度达到数百万字节时，拒绝服务问题才会出现。然而，由于最有用的 LIKE 或 GLOB 模式最多只有几十个字节的长度，
	@静态
	常量 SQLITE最大类模式长度 为 整数 = 50000
	@附加权限(安卓权限.文件权限_写入)
	@静态
	方法 连接数据库(数据库路径 为 文本,打开标志 为 整数 = 268435456) : 数据库管理
		变量 文件1 = 文件.从路径创建(数据库路径)
		变量 父目录 = 文件1.取父目录()
		如果 取反(父目录.存在) 则
			父目录.新建文件夹()
		结束 如果
		code return #<@数据库管理>.openDatabase(#数据库路径,null,#打开标志);
	结束 方法
	@静态
	@运算符重载
	@嵌入式代码
	方法 =(数据库路径 为 文本) : 数据库管理
		code #<数据库管理.连接数据库>(#数据库路径,268435456);
	结束 方法
	//返回数据库可能增长到的最大大小。
	属性读 最大尺寸() 为 长整数
		@code return #this.getMaximumSize();@end
	结束 属性
	//返回当前数据库页面大小，以字节为单位。
	属性读 页面大小() 为 长整数
		@code return #this.getPageSize();@end
	结束 属性
	属性写 最大尺寸(尺寸 为 长整数)
		@code #this.setMaximumSize(#尺寸);@end
	结束 属性
	//返回当前数据库页面大小，以字节为单位。
	属性写 页面大小(大小 为 长整数)
		@code #this.setPageSize(#大小);@end
	结束 属性
	//获取数据库文件的路径。
	属性读 路径() 为 文本
		@code return #this.getPath();@end
	结束 属性
	//获取数据库版本。
	属性读 数据库版本() 为 整数
		@code return #this.getVersion();@end
	结束 属性
	属性写 数据库版本(版本 为 整数)
		@code #this.setVersion(#版本);@end
	结束 属性
	//如果数据库以只读方式打开，则返回 真
	属性读 只读() 为 逻辑型
		@code return #this.isReadOnly();@end
	结束 属性
	//如果为此数据库启用了预写日志记录，则返回 真
	属性读 是否开启预写日志() 为 逻辑型
		@code return #this.isWriteAheadLoggingEnabled();@end
	结束 属性
	/*
   参数 ->表名：执行操作的表名
   参数 ->条件：删除时应用的可选 WHERE 子句。传递 null 将删除所有行。
   参数 ->条件内容替换：替换条件中的“?”,顺序替换(类似格式化文本)
   返回 ：如果传入 [条件]，则受影响的行数，否则为 0。删除所有行并获得计数传递“1”作为 [条件]。
   */
	方法 删除记录(表名 为 文本,条件 为 文本=空,条件内容替换 为 文本[] = 空) 为 整数
		code return #this.delete(#表名,#条件,#条件内容替换);
	结束 方法
	/*
   参数 ->表名：执行操作的表名
   参数 ->可空的字段：可选的; 可能是null。SQL 不允许在没有命名至少一个字段的情况下插入一个完全空的行。如果您提供的[记录]是空的，则没有字段是已知的，并且无法插入空行。如果未设置为 null，则该nullColumnHack参数提供可空字段称的名称，以便在您values为空的情况下显式插入 NULL 。
   参数 ->记录：此映射包含行的初始字段值。键应该是字段，值应该是字段值
   返回 ：新插入行的行 ID，如果发生错误，则为 -1
   */
	方法 插入记录(表名 为 文本,记录 为 数据库记录集,可空的字段 为 文本 = 空) 为 长整数
		@code return #this.insert(#表名,#可空的字段,#记录);@end
	结束 方法
	/*
   参数 ->表名：执行操作的表名
   参数 ->可空的字段：可选的; 可能是null。SQL 不允许在没有命名至少一个字段的情况下插入一个完全空的行。如果您提供的[记录]是空的，则没有字段是已知的，并且无法插入空行。如果未设置为 null，则该nullColumnHack参数提供可空字段称的名称，以便在您values为空的情况下显式插入 NULL 。
   参数 ->记录：此映射包含行的初始字段值。键应该是字段名，值应该是字段值
   参数 ->冲突解决：用于插入冲突解决程序（常量）
   返回 ：新插入行的行 ID 或-1如果输入参数conflictAlgorithm= 或发生错误。CONFLICT_IGNORE
   */
	方法 插入记录2(表名 为 文本,可空的字段 为 文本,记录 为 数据库记录集,冲突解决 为 整数) 为 长整数
		@code return #this.insertWithOnConflict(#表名,#可空的字段,#记录,#冲突解决);@end
	结束 方法
	/*
   参数 ->表名：执行操作的表名
   参数 ->欲查字段列表：要返回的字段的列表。传递 null 将返回所有字段，不鼓励这样做以防止从不会使用的存储中读取数据。
   参数 ->条件：查询时应用的可选 WHERE 子句。传递 null 将查询所有行。
   参数 ->条件内容替换：替换条件中的“?”,顺序替换（类似格式化文本）
   参数 -> 以指定字段过滤重复：声明如何对行进行分组的过滤器[字段名]，格式为 SQL GROUP BY 子句（不包括 GROUP BY 本身）。传递 null 将导致行不被分组。
   参数 -> 二次过滤条件：如果使用字段过滤重复，过滤器声明要包含在游标中的行组[字段 比较符 比较值][字段a > 20]只显示字段a大于20的数据，格式为 SQL HAVING 子句（不包括 HAVING 本身）。传递 null 将导致包含所有行组，并且在不使用行分组时需要。
   参数 -> 排序：次指定字段进行排序[字段 desc/asx]desc倒序，格式化为 SQL ORDER BY 子句（不包括 ORDER BY 本身）。传递 null 将使用默认排序顺序，该顺序可能是无序的。
   参数 -> 返回行数：限制查询返回的行数，格式为 LIMIT 子句。传递 null 表示没有 LIMIT 子句。
   返回 ：新插入行的行 ID 或-1如果输入参数conflictAlgorithm= 或发生错误。CONFLICT_IGNORE
   */
	方法 查询记录(表名 为 文本,欲查字段列表 为 文本[] = 空,条件 为 文本 = 空,条件内容替换 为 文本[]=空,以指定字段过滤重复 为 文本 = 空 ,二次过滤条件 为 文本 = 空 ,排序 为 文本 = 空 ,返回行数 为 文本 = 空) 为 数据库光标
		@code return (#this.query(#表名,#欲查字段列表,#条件,#条件内容替换,#以指定字段过滤重复,#二次过滤条件,#排序,#返回行数));@end
	结束 方法
	/*
   参数 ->表名：执行操作的表名
   参数 ->记录：从字段到新字段值的映射。null 是将被转换为 NULL 的有效值。
   参数 ->条件：更新时应用的可选 WHERE 子句。传递 null 将删除所有行。
   参数 ->条件内容替换：替换条件中的“?”,顺序替换(类似格式化文本)
   返回 ：新插入行的行 ID，如果发生错误，则为 -1
   */
	方法 更新记录(表名 为 文本,记录 为 数据库记录集,条件 为 文本,条件内容替换 为 文本[]) 为 长整数
		@code return #this.update(#表名,#记录,#条件,#条件内容替换);@end
	结束 方法
	//执行不是 SELECT 或任何其他返回数据的 SQL 语句的单个 SQL 语句。它无法返回任何数据（例如受影响的行数）。
	方法 执行SQL(sql 为 文本)
		@code #this.execSQL(#sql);@end
	结束 方法
	
	方法 执行查询SQL(sql 为 文本) 为 数据库光标
		@code return (#this.rawQuery(#sql,null));@end
	结束 方法
	
	//通过编译来验证 SQL SELECT(查询) 语句是否有效。如果 SQL 语句无效则报异常
	方法 验证SQL(sql 为 文本)
		@code #this.validateSql(#sql,null);@end
	结束 方法
	//查找可编辑的第一个表的名称。
	方法 查找可编辑表(表列表 为 文本) 为 文本
		@code return #this.findEditTable(#表列表);@end
	结束 方法
	//如果数据库当前处于打开状态，则返回 真
	属性读 开启() 为 逻辑型
		@code return #this.isOpen();@end
	结束 属性

	//删除数据库，包括其日志文件和其他可能由数据库引擎创建的辅助文件。
	@静态
	方法 删除数据库(路径 为 文本) 为 逻辑型
		@code return SQLiteDatabase.deleteDatabase(new File(#路径));@end
	结束 方法
	//如果新版本代码大于当前数据库版本，则返回 真
	方法 是否需要升级(最新版本号 为 整数) 为 逻辑型
		@code return #this.needUpgrade(#最新版本号);@end
	结束 方法
	
	//自定义代码查询数据库
	方法 取自定义代码执行器() : SQL代码生成
		变量 SQL代码生成1 : SQL代码生成
		code #<SQL代码生成1.绑定>(#this);
		返回 SQL代码生成1
	结束 方法
	
结束 类

@导入Java("android.content.ContentValues")
@指代类("android.content.ContentValues")
类 数据库记录集
	//变量 a 为 集合
	方法 清空()
		code #this.clear();
	结束 方法
	@运算符重载
	方法 ?(键 为 文本) 为 逻辑型
		code return #this.containsKey(#键);
	结束 方法
	@运算符重载
	方法 ==(比较对象 为 对象) 为 逻辑型
		code return #this.equals(#比较对象);
	结束 方法
	@运算符重载
	方法 [](键 : 文本) : 对象
		code return #this.get(#键);
	结束 方法
	@运算符重载
	方法 []=(键 为 文本,值 为 对象)
		 添加项目(键,值)
	结束 方法
	方法 取项目(键 为 文本) 为 对象
		code return #this.get(#键);
	结束 方法
	方法 取逻辑型项目(键 为 文本) 为 逻辑型
		code return #this.getAsBoolean(#键);
	结束 方法
	方法 取字节组项目(键 为 文本) 为 字节[]
		code return #this.getAsByteArray(#键);
	结束 方法
	方法 取字节型项目(键 为 文本) 为 字节
		code return #this.getAsByte(#键);
	结束 方法

	方法 取浮点数型项目(键 为 文本) 为 单精度小数
		code return #this.getAsFloat(#键);
	结束 方法
	方法 取双精度型项目(键 为 文本) 为 小数
		code return #this.getAsDouble(#键);
	结束 方法
	方法 取整数项目(键 为 文本) 为 整数
		code return #this.getAsInteger(#键);
	结束 方法
	方法 取长整数项目(键 为 文本) 为 长整数
		code return #this.getAsLong(#键);
	结束 方法
	方法 取文本项目(键 为 文本) 为 文本
		code return #this.getAsString(#键);
	结束 方法
	方法 为空() 为 逻辑型
		code return #this.isEmpty();
	结束 方法
	方法 添加项目(键 为 文本,值 为 对象)
		@code
	   if(#值 == null){
          #this.putNull(#键);
      }else if(#值 instanceof Integer){
          #this.put(#键,((int)#值));
     }else if(#值 instanceof Long){
          #this.put(#键,((Long)#值));
     }else if(#值 instanceof Boolean){
          #this.put(#键,((Boolean)#值));
     }else if(#值 instanceof Float){
          #this.put(#键,((Float)#值));
     }else if(#值 instanceof Double){
          #this.put(#键,((Double)#值));
     }else if(#值 instanceof Byte){
          #this.put(#键,((Byte)#值));
     }else if(#值 instanceof Short){
          #this.put(#键,((Short)#值));
     }else if(#值 instanceof String){
          #this.put(#键,((String)#值));
      }else{
          #this.put(#键,#值.toString());
      }
	  @end
	结束 方法
	方法 添加项目_字节组(键 为 文本,值 为 字节[])
		@code #this.put(#键,#值);@end
	结束 方法
	方法 添加项目_复制(记录值集 为 数据库记录集)
		@code #this.putAll(#记录值集);@end
	结束 方法
	方法 添加项目_空(键 为 文本)
		@code #this.putNull(#键);@end
	结束 方法
	方法 删除项目(键 为 文本)
		@code #this.remove(#键);@end
	结束 方法
	方法 取项目总数() 为 整数
		@code return #this.size();@end
	结束 方法

结束 类


/*
遍历思路：

变量 光标 为 数据库光标 = ...
判断循环 #this.下一行()
   #this.读取内容(0)//读取当前行指定列的内容
结束 循环
*/
@指代类("android.database.Cursor")
类 数据库光标 

	方法 下一行() 为 逻辑型
		code return #this.moveToNext();
	结束 方法
	
	方法 上一行() 为 逻辑型
		code return #this.moveToPrevious();
	结束 方法
	
	方法 最后行() 为 逻辑型
		code return #this.isAfterLast();
	结束 方法
	
	方法 第一行() 为 逻辑型
		code return #this.isBeforeFirst();
	结束 方法
	
	方法 移动光标位置(数 为 整数) 为 逻辑型
		code return #this.move(#数);
	结束 方法
	
	方法 光标位置() 为 整数
		code return #this.getPosition();
	结束 方法
	
	方法 取列索引(列名 为 文本) 为 整数
		code return #this.getColumnIndex(#列名);
	结束 方法
	
	方法 取文本(键名 : 文本, 默认值 : 文本 = "") : 文本
		开始俘获异常()
		返回 取文本_索引(取列索引(键名))
		俘获所有异常()
		取俘获异常().输出堆栈信息()
		结束俘获异常()
		返回 默认值
	结束 方法

	方法 取整数(键名 : 文本, 默认值 : 整数 = 0) : 整数
		开始俘获异常()
		返回 取整数_索引(取列索引(键名))
		俘获所有异常()
		取俘获异常().输出堆栈信息()
		结束俘获异常()
		返回 默认值
	结束 方法

	方法 取长整数(键名 : 文本,默认值 : 长整数 = 0) : 长整数
		开始俘获异常()
		返回 取长整数_索引(取列索引(键名))
		俘获所有异常()
		取俘获异常().输出堆栈信息()
		结束俘获异常()
		返回 默认值
	结束 方法

	方法 取小数(键名 : 文本,默认值 : 小数 = 0.0d) : 小数
		开始俘获异常()
		返回 取小数_索引(取列索引(键名))
		俘获所有异常()
		取俘获异常().输出堆栈信息()
		结束俘获异常()
		返回 默认值
	结束 方法

	方法 取单精度小数(键名 : 文本,默认值 : 单精度小数 = 0.0f) : 单精度小数
		开始俘获异常()
		返回 取单精度小数_索引(取列索引(键名))
		俘获所有异常()
		取俘获异常().输出堆栈信息()
		返回 默认值
		结束俘获异常()
	结束 方法
	
	方法 取文本_索引(索引 : 整数) : 文本
		code return #this.getString(#索引);
	结束 方法

	方法 取整数_索引(索引 : 整数) : 整数
		code return #this.getInt(#索引);
	结束 方法

	方法 取长整数_索引(索引 : 整数) : 长整数
		code return #this.getLong(#索引);
	结束 方法

	方法 取小数_索引(索引 : 整数) : 小数
		code return #this.getDouble(#索引);
	结束 方法

	方法 取单精度小数_索引(索引 : 整数) : 单精度小数
		code return #this.getFloat(#索引);
	结束 方法
	
	方法 取数据类型(列名 为 文本) 为 整数
		返回 取数据类型_索引(取列索引(列名))
	结束 方法
	
	方法 取数据类型_索引(索引 为 整数) 为 整数
		code return #this.getType(#索引);
	结束 方法
	
	
	
	方法 到第一行() 为 逻辑型
		code return #this.moveToFirst();
	结束 方法
	
	方法 到最后行() 为 逻辑型
		code return #this.moveToLast();
	结束 方法
	
	属性读 总列数() 为 整数
		code return #this.getColumnCount();
	结束 属性
	
	属性读 总行数() 为 整数
		code return #this.getCount();
	结束 属性
	
	属性读 光标是否关闭() 为 逻辑型
		code return #this.isClosed();
	结束 属性
	
	方法 置光标位置(索引 为 整数) 为 逻辑型
		code return #this.moveToPosition(#索引);
	结束 方法

	//关闭光标，释放其所有资源并使其完全无效。
	方法 关闭光标()
		code #this.close();
	结束 方法

结束 类