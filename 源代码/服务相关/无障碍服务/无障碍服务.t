包名 baipiaoguai.wuzhangai
@禁止创建对象
@附加权限("android.permission.CHANGE_CONFIGURATION")
@附加权限("android.accessibilityservice.AccessibilityService")
@附加权限("android.permission.VIBRATE")
@附加权限("android.permission.SYSTEM_ALERT_WINDOW")
@指代类("android.accessibilityservice.AccessibilityService")
@安卓资源.XML("../fw.xml")
类 安卓无障碍服务 : 安卓服务
	方法 取窗口根节点() : 无障碍节点
		code return #this.getRootInActiveWindow();
	结束 方法

	方法 执行全局操作(无障碍服务_全局操作 : 整数)
		code #this.performGlobalAction(#无障碍服务_全局操作);
	结束 方法

	方法 发送手势(手势 : 手势描述,回调 : 手势结果回调 = 空,回调线程 : 手势线程 = 空) : 逻辑型
		code return #this.dispatchGesture(#手势,#回调,#回调线程);
	结束 方法
	/*
	方法 屏幕截图(显示ID : 整数,执行人 : 对象,回调 : 对象)
		//Executor,TakeScreenshotCallback
		code #this.takeScreenshot(#显示ID,#执行人,#回调);
	结束 方法*/
	方法 查找焦点(焦点类型 : 整数) : 无障碍节点
		code return #this.findFocus(#焦点类型);
	结束 方法
	/*
	方法 取服务信息() : 
		code return #this.getServiceInfo();
	结束 方法
	*/
	方法 禁用()
		code #this.disableSelf();
	结束 方法

结束 类
@导入Java("android.text.*")
@导入Java("android.provider.*")
@禁止创建对象
类 无障碍服务 : 安卓无障碍服务
	@code
	    @Override
    protected boolean onKeyEvent(android.view.KeyEvent keyEvent) {
       return #系统按键被单击(keyEvent);
    }
    
    @Override
    public void onAccessibilityEvent(android.view.accessibility.AccessibilityEvent event) {
        #窗口变化回调(event);
    }

    //开启
    @Override
    public void onCreate() {
		#初始化();
        super.onCreate();
    }

    //中断
    @Override
    public void onInterrupt() {
		#关闭();
    }
	//手势
	@Override
    public boolean onGesture(android.accessibilityservice.AccessibilityGestureEvent age) {
		return #屏幕被触摸(age);
    }

    /**
     * 当启动服务的时候就会被调用
     */
    
    @Override
    protected void onServiceConnected() {
     #开启();
     super.onServiceConnected();
    }
	
      public static boolean isAccessibilitySettingsOn(#<@安卓环境> context,#<@Java类> j) {
      int n = 0;
      String resolver;
      String name = context.getPackageName() + "/" + j.getName();
      try {
         n = Settings.Secure.getInt(context.getApplicationContext().getContentResolver(), "accessibility_enabled");
      } catch (Settings.SettingNotFoundException e) {
         e.printStackTrace();
      }
      TextUtils.SimpleStringSplitter splitter = new TextUtils.SimpleStringSplitter(':');
      if (n == 1 && (resolver = Settings.Secure.getString(context.getApplicationContext().getContentResolver(),
      "enabled_accessibility_services")) != null) {
         splitter.setString(resolver);
         while (splitter.hasNext()) {
            String text = splitter.next();
            if (!text.equalsIgnoreCase(name))
            continue;
            return true;
         }
      }
      return false;
   }
   
    @end
	//开启辅助功能
	@静态
	@嵌入式代码
	方法 启用辅助功能(窗口对象 : 安卓窗口,无障碍服务类 : 安卓无障碍服务)
		@code
      #<@启动信息> intent = new #<@启动信息>(#窗口对象,#无障碍服务类.class);
      #窗口对象.startService(intent);
      if (!#<无障碍服务>.isAccessibilitySettingsOn(#窗口对象,#无障碍服务类.class)) {
         try {
            #窗口对象.startActivity(new #<@启动信息>("android.settings.ACCESSIBILITY_SETTINGS"));
         } catch (Exception e) {
            e.printStackTrace();
         }
      }
      @end
	结束 方法
	@静态
	@嵌入式代码
	方法 辅助是否开启(窗口对象 : 安卓窗口,无障碍服务类 : 安卓无障碍服务) : 逻辑型
		code #<无障碍服务>.isAccessibilitySettingsOn(#窗口对象,#无障碍服务类.class)
	结束 方法

	@虚拟方法
	方法 系统按键被单击(按键的事件 : 按键事件) : 逻辑型
		返回 真
	结束 方法
	@虚拟方法
	方法 窗口变化回调(无障碍的事件 : 无障碍事件)
	结束 方法
	@虚拟方法
	方法 屏幕被触摸(手势事件 : 无障碍手势事件) : 逻辑型
		返回 真
	结束 方法
	@虚拟方法
	方法 初始化()
	结束 方法
	@虚拟方法
	方法 关闭()
	结束 方法
	@虚拟方法
	方法 开启()
	结束 方法
结束 类

@指代类("android.accessibilityservice.AccessibilityGestureEvent")
类 无障碍手势事件
	属性读 显示ID() : 整数
		code return #this.getDisplayId();
	结束 属性
	属性读 获取手势Id() : 整数
		code return #this.getGestureId();
	结束 属性
	属性读 描述内容() : 整数
		code return #this.describeContents();
	结束 属性
结束 类

@指代类("android.view.accessibility.AccessibilityEvent")
类 无障碍事件
	属性读 事件发送时间() 为 长整数
		code return #this.getEventTime();
	结束 属性
	属性读 遍历的运动粒度() 为 整数
		code return #this.getMovementGranularity();
	结束 属性
	属性读 程序包名称() 为 字符串
		code return #this.getPackageName();
	结束 属性
	属性读 事件中包含的记录数() 为 整数
		code return #this.getRecordCount();
	结束 属性
	属性读 被执行操作类型() : 无障碍事件_操作类型
		code return #this.getAction();
	结束 属性
	属性读 事件类型() : 整数
		code return #this.getEventType();
	结束 属性
	属性读 内容更改类型() : 无障碍事件_内容改变类型
		code return #this.getContentChangeTypes();
	结束 属性
	属性读 窗口更改类型() : 无障碍事件_窗口改变类型
		code return #this.getWindowChanges();
	结束 属性
	/*public AccessibilityRecord getRecord(int index) {}
*/
结束 类
@强制输出
@指代类("android.view.accessibility.AccessibilityWindowInfo")
类 无障碍窗口
	
	/*getRegionInScreen
Added in API level 30

public void getRegionInScreen (Region outRegion)
Gets the touchable region of this window in the screen.

Parameters
outRegion
Region: The out window region. This value cannot be null.
*/
	/*Describe the kinds of special objects contained in this Parcelable instance's marshaled representation. For example, if the object will include a file descriptor in the output of writeToParcel(android.os.Parcel, int), the return value of this method must include the CONTENTS_FILE_DESCRIPTOR bit.
	返回 ：整数
a bitmask indicating the set of special object types marshaled by this Parcelable object instance. Value is either 0 or CONTENTS_FILE_DESCRIPTOR*/
	属性读 窗口描述() : 整数
		code return #this.describeContents();
	结束 属性
	
	属性读 窗口标题() : 字符串
		code return #this.getTitle();
	结束 属性
	
	属性读 窗口类型() : 整数
		code return #this.getType();
		/*See also:
		TYPE_APPLICATION
		TYPE_INPUT_METHOD
		TYPE_SYSTEM
		TYPE_ACCESSIBILITY_OVERLAY*/
	结束 属性
	
	属性读 窗口节点锚() : 无障碍节点
		code return #this.getAnchor();
	结束 属性
	
	属性读 子窗口数量() : 整数
		code return #this.getChildCount();
	结束 属性
	
	属性读 父窗口() : 无障碍窗口
		code return #this.getParent();
	结束 属性
	
	属性读 窗口显示ID() : 整数
		code return #this.getDisplayId();
	结束 属性
	
	属性读 窗口ID() : 整数
		code return #this.getId();
	结束 属性
	
	属性读 窗口层级() : 整数
		code return #this.getLayer();
	结束 属性
	方法 窗口矩形(矩形1 : 矩形)
		code #this.getBoundsInScreen(#矩形1);
	结束 方法
	
	方法 取子窗口(索引 : 整数) : 无障碍窗口
		code return #this.getChild(#索引);
	结束 方法
	/*
	方法 取窗口根节点(预取策略 : 整数 = 0) : 无障碍节点
		code return #this.getRoot(#预取策略);
	结束 方法*/
	
	属性读 窗口根节点() : 无障碍节点
		code return #this.getRoot();
	结束 属性
	
	属性读 可聚焦() : 逻辑型
		code return #this.isAccessibilityFocused();
	结束 属性
	
	属性读 是当前活动窗口() : 逻辑型
		code return #this.isActive();
	结束 属性
	
	属性读 输入焦点() : 逻辑型
		code return #this.isFocused();
	结束 属性
	
	属性读 是画中画模式() : 逻辑型
		code return #this.isInPictureInPictureMode();
	结束 属性
	
	方法 回收资源()
		code #this.recycle();
	结束 方法
	@静态
	方法 获得实例对象() : 无障碍窗口
		code return #<@无障碍窗口>.obtain();
	结束 方法
	
结束 类

@指代类("android.view.accessibility.AccessibilityNodeInfo")
类 无障碍节点
	属性读 窗口ID() : 整数
		code return #this.getWindowId();
	结束 属性
	属性读 子节点数量() : 整数
		code return #this.getChildCount();
	结束 属性
	方法 取子节点(索引 : 整数) : 无障碍节点
		code return #this.getChild(#索引);
	结束 方法
	属性读 之前遍历() : 无障碍节点
		code return #this.getTraversalBefore();
	结束 属性
	属性读 之后遍历() : 无障碍节点
		code return #this.getTraversalAfter();
	结束 属性
	属性读 获取可用的额外数据() : 文本集合
		code return new #<@文本集合>(#this.getAvailableExtraData());
	结束 属性
	属性读 获取最大文本长度() : 整数
		code return #this.getMaxTextLength();
	结束 属性
	属性读 获取移动粒度() : 整数
		code return #this.getMovementGranularities();
	结束 属性
	属性读 窗口() : 无障碍窗口
		code return #this.getWindow();
	结束 属性
	属性读 父节点() : 无障碍节点
		code return #this.getParent();
	结束 属性
	属性读 可检查() : 逻辑型
		code return #this.isCheckable();
	结束 属性
	属性读 已选中() : 逻辑型
		code return #this.isChecked();
	结束 属性
	属性读 可聚焦() : 逻辑型
		code return #this.isFocusable();
	结束 属性
	属性读 聚焦() : 逻辑型
		code return #this.isFocused();
	结束 属性
	属性读 对用户可见() : 逻辑型
		code return #this.isVisibleToUser();
	结束 属性
	属性读 以无障碍为中心() : 逻辑型
		code return #this.isAccessibilityFocused();
	结束 属性
	属性读 已选定() : 逻辑型
		code return #this.isSelected();
	结束 属性
	属性读 可点击() : 逻辑型
		code return #this.isClickable();
	结束 属性
	属性读 可长按() : 逻辑型
		code return #this.isLongClickable();
	结束 属性
	属性读 已启用() : 逻辑型
		code return #this.isEnabled();
	结束 属性
	属性读 是密码() : 逻辑型
		code return #this.isPassword();
	结束 属性
	属性读 可滚动() : 逻辑型
		code return #this.isScrollable();
	结束 属性
	属性读 可编辑() : 逻辑型
		code return #this. isEditable();
	结束 属性
	属性读 窗格标题() : 字符串
		code return #this.getPaneTitle();
	结束 属性
	属性读 绘图顺序() : 整数
		code return #this.getDrawingOrder();
	结束 属性
	属性读 内容无效() : 逻辑型
		code return #this.isContentInvalid();
	结束 属性
	属性读 上下文可单击() : 逻辑型
		code return #this.isContextClickable();
	结束 属性
	属性读 实时区域() : 整数
		code return #this.getLiveRegion();
	结束 属性
	属性读 是多线的() : 逻辑型
		code return #this.isMultiLine();
	结束 属性
	属性读 是否打开弹出窗口或对话框() : 逻辑型
		code return #this.canOpenPopup();
	结束 属性
	属性读 可取消() : 逻辑型
		code return #this.isDismissable();
	结束 属性
	属性读 对无障碍性很重要() : 逻辑型
		code return #this.isImportantForAccessibility();
	结束 属性
	属性读 可聚焦屏幕阅读器() : 逻辑型
		code return #this.isScreenReaderFocusable();
	结束 属性
	属性读 显示提示文本() : 逻辑型
		code return #this.isShowingHintText();
	结束 属性
	属性读 是文本输入键() : 逻辑型
		code return #this.isTextEntryKey();
	结束 属性
	属性读 包名称() : 文本
		code return #this.getPackageName().toString();
	结束 属性
	属性读 类名称() : 文本
		code return #this.getClassName().toString();
	结束 属性
	属性读 文本内容() : 文本
		code return #this.getText().toString();
	结束 属性

	属性读 提示文本内容() : 字符串
		code return #this.getHintText();
	结束 属性
	属性读 错误文本() : 字符串
		code return #this.getError();
	结束 属性
	属性读 状态描述() : 字符串
		code return #this.getStateDescription();
	结束 属性
	属性读 内容描述() : 字符串
		code return #this.getContentDescription();
	结束 属性
	属性读 工具提示文本() : 字符串
		code return #this.getTooltipText();
	结束 属性
	属性读 标签用于() : 无障碍节点
		code return #this.getLabelFor();
	结束 属性
	属性读 标签依据() : 无障碍节点
		code return #this.getLabeledBy();
	结束 属性
	属性读 获取视图ID资源() : 字符串
		code return #this.getViewIdResourceName();
	结束 属性
	属性读 获取文本选择开始() : 整数
		code return #this.getTextSelectionStart();
	结束 属性
	属性读 获取文本选择结束() : 整数
		code return #this.getTextSelectionEnd();
	结束 属性
	属性读 输入类型() : 整数
		code return #this.getInputType();
	结束 属性

	方法 置控件内容(欲设置内容 为 文本)
		@code
      try {
         android.os.Bundle bundle = new android.os.Bundle();
         bundle.putCharSequence("ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE", #欲设置内容);
         #this.performAction(2097152, bundle);
      } catch (Exception e) {
         e.printStackTrace();
      }
      @end
	结束 方法

	方法 取控件位置(保存的坐标点 : 矩形)
		code #this.getBoundsInScreen(#保存的坐标点);
	结束 方法


	/*
	属性读 获取附加服务() : Bundle
		code return #this.getExtras()
	结束 属性*/

	/*
List<AccessibilityNodeInfo.AccessibilityAction>
getActionList()
获取可以在节点上执行的操作。
public TouchDelegateInfo getTouchDelegateInfo() {}
public CollectionInfo getCollectionInfo() {}
	public CollectionItemInfo getCollectionItemInfo() {}
	public RangeInfo getRangeInfo() {}
	public ExtraRenderingInfo getExtraRenderingInfo() {}
*/
	方法 执行操作(操作类型 : 整数)
		code #this.performAction(#操作类型);
	结束 方法

	方法 查询_文本(内容 : 文本) : 集合
		code return new #<@集合>(#this.findAccessibilityNodeInfosByText(#内容));
	结束 方法
	方法 查询_ID(视图ID : 文本) : 集合
		code return new #<@集合>(#this.findAccessibilityNodeInfosByViewId(#视图ID));
	结束 方法

结束 类