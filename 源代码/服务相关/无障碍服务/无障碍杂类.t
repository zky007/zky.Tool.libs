@禁止创建对象
@需求值类型(整数)
@常量类型(整数)
类 无障碍事件_操作类型
	@静态
	常量 公告 : 整数 = 16384
	@静态
	常量 辅助阅读上下文 : 整数 = 16777216
	@静态
	常量 手势检测结束 : 整数 = 524288
	@静态
	常量 手势检测开始 : 整数 = 262144
	@静态
	常量 通知状态更改 : 整数 = 64
	@静态
	常量 触摸探索手势开始 : 整数 = 512
	@静态
	常量 触摸探索手势结束 : 整数 = 1024
	@静态
	常量 触摸检测开始 : 整数 = 1048576
	@静态
	常量 触摸检测结束 : 整数 = 2097152
	@静态
	常量 可获得焦点 : 整数 = 32768
	@静态
	常量 焦点已清除 : 整数 = 65536
	@静态
	常量 单击 : 整数 = 1
	@静态
	常量 长按 : 整数 = 2
	@静态
	常量 点击上下文 : 整数 = 8388608
	@静态
	常量 聚焦 : 整数 = 8
	@静态
	常量 暂停进入 : 整数 = 128
	@静态
	常量 暂停退出 : 整数 = 256
	
	@静态
	常量 滚动 : 整数 = 4096
	@静态
	常量 选择 : 整数 = 4
	@静态
	常量 文本被改变 : 整数 = 16
	@静态
	常量 选中文本被改变 : 整数 = 8192
	@静态
	常量 文本光标被改变 : 整数 = 131072
	@静态
	常量 窗口被改变 : 整数 = 4194304
	@静态
	常量 窗口内容被改变 : 整数 = 2048
	@静态
	常量 窗口状态被改变 : 整数 = 32
	
	/*public final static int TYPES_ALL_MASK = -1;
	？ int 类型_视图TEXT_TRAVERSED_AT_MOVEMENT_GRANULARITY = 131072;
*/
结束 类
@禁止创建对象
@需求值类型(整数)
@常量类型(整数)
类 无障碍事件_窗口改变类型
	@静态
	常量 窗口可焦点 : 整数 = 128
	@静态
	常量 窗口被改变 : 整数 = 32
	@静态
	常量 窗口被添加 : 整数 = 1
	@静态
	常量 窗口被绑定 : 整数 = 8
	@静态
	常量 窗口焦点发生改变 : 整数 = 64
	@静态
	常量 窗口图层被改变 : 整数 = 16
	@静态
	常量 窗口父级被改变 : 整数 = 256
	@静态
	常量 窗口管道被改变 : 整数 = 1024
	@静态
	常量 窗口被关闭 : 整数 = 2
	@静态
	常量 窗口标题被改变 : 整数 = 4
	@静态
	常量 窗口子级被改变 : 整数 = 512
	/*
	public final static int 窗口_CHANGE_CHILDREN = 512;
	public final static int 窗口_CHANGE_LAYER = 16;
	public final static int 窗口_CHANGE_PARENT = 256;
	public final static int 窗口_CHANGE_PIP = 1024;
	*/
结束 类
@禁止创建对象
@需求值类型(整数)
@常量类型(整数)
类 无障碍事件_内容改变类型
	@静态
	常量 未定义 : 整数 = 0
	@静态
	常量 树 : 整数 = 1
	@静态
	常量 文本 : 整数 = 2
	@静态
	常量 描述内容 : 整数 = 4
	@静态
	常量 面板标题 : 整数 = 8
	@静态
	常量 面板外观 : 整数 = 16
	@静态
	常量 面板显示 : 整数 = 32
	@静态
	常量 状态描述 : 整数 = 64
结束 类
@禁止创建对象
@需求值类型(整数)
@常量类型(整数)
类 无障碍服务_全局操作
	@静态
	常量 返回键 : 整数 = 1
	@静态
	常量 主页 : 整数 = 2
	@静态
	常量 最近 : 整数 = 3
	@静态
	常量 显示通知栏 : 整数 = 4
	@静态
	常量 打开设置 : 整数 = 5
	@静态
	常量 电源对话框 : 整数 = 6
	@静态
	常量 拨动分屏 : 整数 = 7
	@静态
	常量 锁屏 : 整数 = 8
	@静态
	常量 屏幕截图 : 整数 = 9
结束 类
@禁止创建对象
@需求值类型(整数)
@常量类型(整数)
类 无障碍节点_操作类型
	@静态 
	常量 获得焦点 为 整数 = 64
   @静态
   常量 失去焦点 为 整数 = 128
   @静态 
   常量 获得输入焦点 为 整数 = 1
   @静态
    常量 失去输入焦点 为 整数 = 2
   @静态 
   常量 选择 为 整数 = 4
   @静态 
   常量 取消选择 为 整数 = 8
   @静态
   常量 设置选择的动作 为 整数 = 131072
   @静态 
   常量 单击 为 整数 = 16
   @静态 
   常量 长按 为 整数 = 32
   @静态 
   常量 拆叠 为 整数 = 524288
   @静态 
   常量 扩展 为 整数 = 262144
   @静态
   常量 复制 为 整数 = 16384
   @静态
   常量 剪切 为 整数 = 65536
   @静态 
   常量 粘粘 为 整数 = 32768
   @静态 
   常量 取消可撒消 为 整数 = 1048576
   @静态
   常量 向前滚动 为 整数 = 4096
   @静态 
   常量 向后滚动 为 整数 = 8192
   @静态 
   常量 设置文本操作 为 整数 = 2097152
   //ACTION_NEXT_AT_MOVEMENT_GRANULARITY
   @静态
   常量 操作_外部_移动_粒度 : 整数 = 256
   //ACTION_NEXT_HTML_ELEMENT
   @静态
   常量 操作_外部_HTML_元素 : 整数 = 1024
   //ACTION_PREVIOUS_AT_MOVEMENT_GRANULARITY
   @静态
   常量 行动_审查_行动_粒度 : 整数 = 512
   //ACTION_PREVIOUS_HTML_ELEMENT
   @静态
   常量 操作_审查_HTML_元素 : 整数 = 2048
   
结束 类
类 无障碍节点_集合 : 集合模板类<无障碍节点>
结束 类
类 无障碍清单配置
	/*
<service android:label="@string/app_name" android:name="zky.mylibs.wza.wzafw" android:permission="android.permission.BIND_ACCESSIBILITY_SERVICE" android:enabled="true" android:exported="true">
			<intent-filter>
				<action android:name="android.accessibilityservice.AccessibilityService" />
			</intent-filter>
			<meta-data android:name="android.accessibilityservice" android:resource="@xml/access_service_config" />
		</service>
*/

	@附加清单.组件配置([[@附加清单.组件配置([[<intent-filter>
				<action android:name="android.accessibilityservice.AccessibilityService" />
			</intent-filter>
			<meta-data android:name="android.accessibilityservice" android:resource="@xml/fw" />
			]])
	@附加清单.组件属性([[android:permission="android.permission.BIND_ACCESSIBILITY_SERVICE" android:enabled="true" android:exported="true"]])
	@静态
	方法 一键配置()
	结束 方法
结束 类