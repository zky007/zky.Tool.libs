包名 白嫖怪.无障碍
@禁止继承
//@禁止创建对象
@输出名("wzafw")
@附加清单.组件配置([[<intent-filter>
				<action android:name="android.accessibilityservice.AccessibilityService" />
			</intent-filter>
			<meta-data android:name="android.accessibilityservice" android:resource="@xml/fw" />
			]])
	@附加清单.组件属性([[android:permission="android.permission.BIND_ACCESSIBILITY_SERVICE" android:enabled="true" android:exported="true"]])
类 无障碍服务_单例模式 : 无障碍服务
	@虚拟方法
	方法 系统按键被单击(按键的事件 : 按键事件) : 逻辑型
		循环(事件列表 -> 事件类)
			变量 wza : 自定义无障碍?
			wza = (事件类 : 自定义无障碍)
			变量 逻辑 : 逻辑型类 = wza.系统按键被单击(按键的事件)
			如果 逻辑 == 空 则
				返回 真
			否则
				返回 逻辑.到基本类型()
			结束 如果
		结束 循环
		返回 真
	结束 方法

	@虚拟方法
	方法 窗口变化回调(无障碍的事件 : 无障碍事件)
		无障碍 = 本对象
		循环(事件列表 -> 事件类)
			变量 wza : 自定义无障碍?
			wza = (事件类 : 自定义无障碍)
			wza.窗口变化回调(无障碍的事件)
		结束 循环
	结束 方法

	@虚拟方法
	方法 屏幕被触摸(手势事件 : 无障碍手势事件) : 逻辑型
		循环(事件列表 -> 事件类)
			变量 wza : 自定义无障碍?
			wza = (事件类 : 自定义无障碍)
			变量 逻辑 : 逻辑型类 = wza.屏幕被触摸(手势事件)
			如果 逻辑 == 空 则
				返回 真
			否则
				返回 逻辑.到基本类型()
			结束 如果
		结束 循环
		返回 真
	结束 方法

	@虚拟方法
	方法 初始化()
		父对象.初始化()
		循环(事件列表 -> 事件类)
			变量 wza : 自定义无障碍?
			wza = (事件类 : 自定义无障碍)
			wza.初始化()
		结束 循环
		
	结束 方法

	@虚拟方法
	方法 关闭()
		无障碍 = 空
		父对象.关闭()
		循环(事件列表 -> 事件类)
			变量 wza : 自定义无障碍?
			wza = (事件类 : 自定义无障碍)
			wza.关闭()
		结束 循环
	结束 方法

	@虚拟方法
	方法 开启()
		无障碍 = 本对象
		父对象.开启()
		循环(事件列表 -> 事件类)
			变量 wza : 自定义无障碍?
			wza = (事件类 : 自定义无障碍)
			wza.开启()
		结束 循环
	结束 方法
	@静态
	@隐藏
	变量 事件列表 : 集合
	@静态
	变量 无障碍 : 无障碍服务_单例模式?

	@禁止调用
	@静态
	方法 添加事件(单例模式 : 自定义无障碍)
		事件列表.添加成员(单例模式)
	结束 方法
	@禁止调用
	@静态
	方法 删除事件(单例模式 : 自定义无障碍)
		事件列表.删除成员2(单例模式)
	结束 方法

	@静态
	方法 获取实例() : 无障碍服务_单例模式
		返回 无障碍
	结束 方法


结束 类
/*
@附加可变清单([[        <service
            android:name="baipiaoguai.wuzhangai.wzafw"
            android:permission="android.permission.BIND_ACCESSIBILITY_SERVICE"
            android:enabled="true"
            android:exported="true">
            <intent-filter>
                <action
                    android:name="android.accessibilityservice.AccessibilityService" />
            </intent-filter>
            <meta-data
                android:name="android.accessibilityservice"
                android:resource="@xml/fw" />
        </service>]])
		*/
类 自定义无障碍 : 窗口组件
	
	@code
	public #<自定义无障碍<(android.content.Context context) {
        super(context);
		#<无障碍服务_单例模式.添加事件>(this);
    }
	@end
	//用于打开无障碍权限列表
	方法 启用辅助功能()
		无障碍服务.启用辅助功能(取安卓窗口(),无障碍服务_单例模式)
	结束 方法
	//用于判断辅助是否被开启
	方法 辅助是否开启() : 逻辑型
		返回 无障碍服务.辅助是否开启(取安卓窗口(),无障碍服务_单例模式)
	结束 方法
	//把当前对象挂接的事件从总部删除，一般在也不需要用到这个对象时使用
	方法 释放资源()
		code #<无障碍服务_单例模式.删除事件>(this);
	结束 方法
	
	方法 取屏幕触摸工具() : 屏幕的触摸
		变量 屏幕的触摸1 : 屏幕的触摸 = 无障碍服务_单例模式.无障碍
		返回 屏幕的触摸1
	结束 方法
	
	定义事件 开启()
	定义事件 关闭()
	定义事件 初始化()

	定义事件 系统按键被单击(按键的事件 : 按键事件) : 逻辑型类
	定义事件 窗口变化回调(无障碍的事件 : 无障碍事件)
	定义事件 屏幕被触摸(手势事件 : 无障碍手势事件) : 逻辑型类
结束 类