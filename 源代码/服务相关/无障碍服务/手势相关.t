@禁止创建对象
@指代类("android.accessibilityservice.GestureDescription")
类 手势描述
	@静态
	方法 获取最大触摸次数()
		code return #<@手势描述>.getMaxStrokeCount();
	结束 方法
	@静态
	方法 获取最大手势持续时间()
		code return #<@手势描述>.getMaxGestureDuration();
	结束 方法
	属性读 触摸次数() : 整数
		code return #this.getStrokeCount();
	结束 属性
	方法 获取触摸(索引 : 整数) : 手势触摸
		code return #this.getStroke(#索引);
	结束 方法
	方法 获取显示ID() : 整数
		code return #this.getDisplayId();
	结束 方法
结束 类
@指代类("android.accessibilityservice.GestureDescription.Builder")
类 手势描述_构建器
	
	方法 添加触摸手势(触摸 : 手势触摸)
		code #this.addStroke(#触摸);
	结束 方法
	方法 设置显示ID(ID : 整数)
		code #this.setDisplayId(#ID);
	结束 方法
	方法 构建() : 手势描述
		code return #this.build();
	结束 方法
结束 类
@指代类("android.accessibilityservice.GestureDescription.StrokeDescription")
类 手势触摸
	@运算符重载
	方法 =(触摸路径 : 触摸路径,延迟 : 整数,持续时间 : 长整数,期间 : 逻辑型) : 手势触摸
		code return new #<@手势触摸>(#触摸路径,#延迟,#持续时间,#期间);
	结束 方法
	属性读 触摸路径() : 触摸路径
		code return #this.getPath();
	结束 属性
	
	属性读 开始时间() : 整数
		code return #this.getStartTime();
	结束 属性
	
	属性读 获取持续时间() : 长整数
		code return #this.getDuration();
	结束 属性
	
	方法 继续() : 逻辑型
		code return #this.willContinue();
	结束 方法
	/*
	方法 继续冲程()
		public StrokeDescription continueStroke(Path path, long startTime, long p3, boolean duration
	结束 方法
	*/
结束 类
@强制输出
@后缀代码("extends android.accessibilityservice.AccessibilityService.GestureResultCallback")
类 手势结果回调
	@输出名("onCompleted")
	定义事件 完成(描述 : 手势描述)
	@输出名("onCancelled")
	定义事件 取消(描述 : 手势描述)
结束 类
@指代类("android.os.Handler")
类 手势线程
	
结束 类
@指代类("android.graphics.Path")
类 触摸路径
	方法 移动到(X : 单精度小数,Y : 单精度小数)
		code #this.moveTo(#X,#Y);
	结束 方法
	
	方法 行至(X : 单精度小数,Y : 单精度小数)
		code #this.lineTo(#X,#Y);
	结束 方法
结束 类