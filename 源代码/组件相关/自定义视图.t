@导入Java("android.content.*")
@导入Java("android.content.res.*")
@导入Java("android.view.*")
@导入Java("android.widget.*")
类 自定义组件 : 可视化组件

	@code
	public #<自定义组件>(Context context) {
		super(context);
	}
	@Override
	public View onCreateView(Context context) {
		View view = new View(context) {
			@Override
			protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
				super.onMeasure(widthMeasureSpec, heightMeasureSpec);
				#被测量(widthMeasureSpec, heightMeasureSpec);
			}
			
			@Override
			protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
				super.onLayout(changed, left, top, right, bottom);
				#被布局(changed, left, top, right - left, bottom - top);
			}
			
			@Override
			protected void onSizeChanged(int w, int h, int oldw, int oldh) {
				super.onSizeChanged(w, h, oldw, oldh);
				#被改变(w, h, oldw, oldh);
			}
			
			@Override
			protected void onDraw(android.graphics.Canvas canvas) {
				super.onDraw(canvas);
				#绘制操作(canvas);
			}
			
			@Override
   	 	public boolean onTouchEvent(#<@触摸事件> event) {
				return #触摸式事件(event);
   		 }
	
			
		};
		return view;
	}
	
	@Override
	public View getView() {
		return view;
	}
	@end

	事件 自定义组件:创建完毕()

	结束 事件	

	@虚拟方法
	方法 被测量(宽度 : 整数, 高度 : 整数)
	结束 方法

	@虚拟方法
	方法 被布局(是否变化 : 逻辑型, 左 : 整数, 上 : 整数, 宽度 : 整数, 高度 : 整数)
	结束 方法

	@虚拟方法
	方法 被改变(新宽度 : 整数, 新高度 : 整数, 旧宽度 : 整数, 旧高度 : 整数)
	结束 方法

	@虚拟方法
	方法 绘制操作(画布 : 画布对象)
	结束 方法

	@虚拟方法
	方法 触摸式事件(触摸 : 触摸事件) : 逻辑型
		返回 真
	结束 方法
结束 类

类 网格文本组件 : 自定义组件


	@code
	public #<网格文本组件>(android.content.Context context) {
		super(context);
		#初始化();
	}
	@end
	//
	@隐藏
	变量 行数 : 整数 = 10
	@隐藏
	变量 列数 : 整数 = 10
	@隐藏
	变量 单元格边长 : 整数 = 100
	@隐藏
	变量 字体大小 : 整数 = 25
	@隐藏
	变量 网格画笔 : 画笔对象 = 画笔对象.创建画笔()
	@隐藏
	变量 字体画笔 : 画笔对象 = 画笔对象.创建画笔()
	@隐藏
	变量 缩放大小 : 单精度小数 = 2f
	@隐藏
	变量 平移X : 单精度小数 = 0f
	@隐藏
	变量 平移Y : 单精度小数 = 0f
	变量 网格文字 : 文本[10][10]
	code android.graphics.Matrix 缩放矩阵 = new android.graphics.Matrix();

	//
	@虚拟方法
	方法 被布局(是否变化 : 逻辑型,左 : 整数,上 : 整数,宽度 : 整数,高度 : 整数)
		父对象.被布局(是否变化,左,上,宽度,高度)
	结束 方法

	@虚拟方法
	方法 绘制操作(画布 : 画布对象)
		父对象.绘制操作(画布)
		画布.保存()

		@code
		// 应用缩放和平移变换
        缩放矩阵.setTranslate(#平移X, #平移Y);
        缩放矩阵.postScale(#缩放大小, #缩放大小);
        #画布.concat(缩放矩阵);
		@end

		循环(行, 0, 行数)
			循环(列, 0, 列数)
				//画网格
				画布.画线(列 * 单元格边长, 0 , 列 * 单元格边长 , 行数 * 单元格边长 ,网格画笔)
				画布.画线(0 , 行 * 单元格边长 , 列数 * 单元格边长 , 行 * 单元格边长 ,网格画笔)
				//画文字
				变量 内容 : 文本 = 网格文字[行][列]
				变量 文字矩形 : 矩形 = 字体画笔.测量文字界限(内容)
				变量 x : 单精度小数 = (列*单元格边长) + ((单元格边长/2) - (文字矩形.宽度()/2))
				变量 y : 单精度小数 = (行*单元格边长) + ((单元格边长/2) - 文字矩形.高度())

				如果 文字矩形.宽度() > 单元格边长/3 则
					画布.画文字(x+25,y-(文字矩形.高度()/2),内容.取文本中间(0,内容.长度/3),字体画笔)
					画布.画文字(x+25,y-(文字矩形.高度()/2)+文字矩形.高度(),内容.取文本中间((内容.长度/3)+1,内容.长度-1),字体画笔)
				否则
					画布.画文字(x,y,内容,字体画笔)
				结束 如果
				
				



			结束 循环
		结束 循环

		//画结尾线
		画布.画线(0 , 行数 * 单元格边长 , 列数 * 单元格边长 , 行数 * 单元格边长 ,网格画笔)
		画布.画线(列数 * 单元格边长, 0 , 列数 * 单元格边长 , 行数 * 单元格边长 ,网格画笔)

		画布.恢复()
	结束 方法

	@隐藏
	变量 原按下X : 单精度小数 = 0f
	@隐藏
	变量 原按下Y : 单精度小数 = 0f
	@隐藏
	变量 是否移动 : 逻辑型 = 假
	@虚拟方法
	方法 触摸式事件(触摸 : 触摸事件) : 逻辑型

		如果 触摸.触摸点数量 == 1 则
			//单指触摸

			如果 触摸.动作 == 触摸动作.按下 则
				是否移动 = 假
				原按下X = (触摸.取横坐标() : 单精度小数)
				原按下Y = (触摸.取纵坐标() : 单精度小数)
			否则 触摸.动作 == 触摸动作.移动
				是否移动 = 真
				变量 x : 单精度小数 = (触摸.取横坐标() : 单精度小数)
				变量 y : 单精度小数 = (触摸.取纵坐标() : 单精度小数)
				平移X = （平移X + ( x -原按下X) / 缩放大小)
				平移Y = (平移Y + ( y - 原按下Y) / 缩放大小)

				原按下X = x
				原按下Y = y
				本对象.刷新()
			否则 触摸.动作 == 触摸动作.抬起
				如果 是否移动 == 假 则

					变量 列索引 : 整数 = (((原按下X - (平移X * 缩放大小) ) / (单元格边长 * 缩放大小)) : 整数)
					变量 行索引 : 整数 = (((原按下Y - (平移Y * 缩放大小) ) / (单元格边长 * 缩放大小)) : 整数)
					如果 列索引 >= 0 && 行索引 >= 0 && 列索引 < 列数 && 行索引 < 行数 则
						网格被单击(行索引,列索引)
					结束 如果

				结束 如果

			结束 如果

		否则
			//多指触摸

		结束 如果

		返回 真
	结束 方法
	@隐藏
	方法 初始化()
		网格画笔.颜色值 = 黑色
		网格画笔.宽度 = 2f
		字体画笔.文字大小 = 23
		字体画笔.颜色值 = 红色
		初始化数组()
	结束 方法

	@隐藏
	方法 初始化数组()
		变量 临时 : 文本[行数][列数]
		循环(x, 0, 行数)
			循环(y, 0, 列数)
				临时[x][y] = "内容("+x+","+y+")"
			结束 循环
		结束 循环
		网格文字 = 临时
	结束 方法
	
	方法 置文字(X索引 : 整数,Y索引 : 整数,内容 : 文本)
		网格文字[X索引][Y索引] = 内容
	结束 方法


	属性写 缩放(缩放大小 : 单精度小数)
		本对象.缩放大小 = 缩放大小
		本对象.刷新()
	结束 属性

	属性读 缩放() : 单精度小数
		返回 缩放大小
	结束 属性

	方法 平移(X : 单精度小数 , Y : 单精度小数)
		平移X = X
		平移Y = Y
		本对象.刷新()
	结束 方法

	属性写 列数(数量 : 整数)
		列数 = 数量
		初始化数组()
		本对象.刷新()
	结束 属性
	属性读 列数() : 整数
		返回 列数
	结束 属性

	属性写 行数(数量 : 整数)
		行数 = 数量
		初始化数组()
		本对象.刷新()
	结束 属性
	属性读 行数() : 整数
		返回 行数
	结束 属性

	属性写 宫格边长(长度 : 整数)
		单元格边长 = 长度
		本对象.刷新()
	结束 属性
	属性读 宫格边长() : 整数
		返回 单元格边长
	结束 属性

	属性写 字体大小(大小 : 整数)
		字体大小 = 字体大小
		本对象.刷新()
	结束 属性
	属性读 字体大小() : 整数
		返回 字体大小
	结束 属性



	定义事件 网格被单击(行索引 : 整数,列索引 : 整数)

结束 类

@导入Java("android.content.*")
@导入Java("android.content.res.*")
@导入Java("android.view.*")
@导入Java("android.widget.*")
@外部Java文件("../../../文档/网格视图.java")
类 测试视图 : 可视化组件
	@code
	public #<测试视图>(Context context) {
		super(context);
	}
	@Override
	public View onCreateView(Context context) {
	return new a.aa.aaa.网格视图(context,null);
	}
	
	@end
结束 类