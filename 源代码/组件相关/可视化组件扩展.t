@导入Java("android.view.View")
类 可视化组件扩展
	//获取组件 宽度高度设置为-2 时的实际大小 返回整数[]，0为宽度，1为高度
		@静态
	方法 获取实际大小(组件 : 可视化组件) : 整数[]
		@code
		View view = #组件.getView();
        int size[] = new int[2];
        int width = View.MeasureSpec.makeMeasureSpec(0,
                View.MeasureSpec.UNSPECIFIED);
        int height = View.MeasureSpec.makeMeasureSpec(0,
                View.MeasureSpec.UNSPECIFIED);
        view.measure(width, height);
        size[0] = view.getMeasuredWidth();
        size[1] = view.getMeasuredHeight();
        return size;
		@end
	结束 方法
	
	@静态
	方法 置组件最大宽度(组件 : 可视化组件,宽度 : 整数)
		code #组件.getView().setMaxWidth(#宽度);
	结束 方法
	
	@静态
	方法 置组件最大高度(组件 : 可视化组件,高度 : 整数)
		code #组件.getView().setMaxHeight(#高度);
	结束 方法
	
	
结束 类

类 编辑框扩展
	变量 编辑框1 : 编辑框?
	
	方法 绑定编辑框(编辑框组件 : 编辑框)
		编辑框1 = 编辑框组件
	结束 方法
	
	方法 置输入法回车为搜索()
		编辑框1.单行输入 = 真
		@code
		#编辑框1.getView().setImeOptions(android.view.inputmethod.EditorInfo.IME_ACTION_SEARCH);
		//#编辑框1.getView().setInputType(android.text.InputType.TYPE_CLASS_TEXT);
		#编辑框1.getView().setOnEditorActionListener(new android.widget.TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(android.widget.TextView v, int actionId, android.view.KeyEvent event) {
                // 当用户点击搜索图标时（actionId为IME_ACTION_SEARCH）
                if (actionId == android.view.inputmethod.EditorInfo.IME_ACTION_SEARCH) {
                    // 在这里执行搜索操作
                    // 例如，调用某个搜索方法
                    #输入法回车键被单击();
                    return true;
                }
                return false;
            }
        });
		
		@end
	结束 方法
	
	定义事件 输入法回车键被单击()
结束 类
/*
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // 获取EditText实例
        EditText editText = findViewById(R.id.myEditText);

        // 设置imeOptions为actionSearch，以显示搜索图标
        editText.setImeOptions(EditorInfo.IME_ACTION_SEARCH);

        // 设置输入类型为文本
        editText.setInputType(InputType.TYPE_CLASS_TEXT);

        // 设置单行输入，防止回车键换行
        editText.setSingleLine(true);

        // 为EditText设置onEditorActionListener
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                // 当用户点击搜索图标时（actionId为IME_ACTION_SEARCH）
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    // 在这里执行搜索操作
                    // 例如，调用某个搜索方法
                    search();
                    return true;
                }
                return false;
            }
        });
    }

    // 搜索方法示例
    private void search() {
        EditText editText = findViewById(R.id.myEditText);
        String searchText = editText.getText().toString();
        // 使用searchText执行搜索逻辑
        // ...
    }
}

*/

//该类不必依靠单选框布局既可实现单选框的关联
类 单选框扩展
	@隐藏
	变量 已选中项 : 复合按钮 = 空
	
	方法 绑定复合按钮(组件 : 复合按钮)
		@code
			((android.widget.CompoundButton) #组件.getView())
			.setOnCheckedChangeListener(
				new android.widget.CompoundButton.OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(android.widget.CompoundButton btn, boolean checked) {
				if (checked) {
					#内部_改变选中状态(new #<可视化组件>(btn));
					#单选框被选中((#<单选框>) btn.getTag());
					#已选中项 = (#<单选框>) btn.getTag();
				}
			}
		});
		
		@end
	结束 方法
	
	@隐藏
	方法 内部_改变选中状态(组件 : 可视化组件)
		变量 父组件 : 布局组件 = 组件.取父组件()
		变量 组件数量 : 整数 = 父组件.取子组件数量()
		循环(索引, 0, 组件数量)
			变量 组件对象 : 可视化组件 = 父组件.取子组件(索引)
			如果 组件对象 属于 复合按钮  则
				变量 复合按钮1 : 复合按钮 = (组件对象 : 复合按钮)
				code #<复合按钮1.选中( (#复合按钮1.getView() == #组件.getView());
			结束 如果
		结束 循环
	结束 方法
	
	
	定义事件 单选框被选中(组件 : 单选框)
结束 类

类 开关扩展
	@静态
	方法 置开关颜色(开关组件 : 开关,颜色 : 整数)
		code #开关组件.getView().setThumbTintList(android.content.res.ColorStateList.valueOf(#颜色));
	结束 方法
	@静态
	方法 颜色2(开关组件 : 开关,颜色 : 整数)
		code #开关组件.getView().setTrackTintList(android.content.res.ColorStateList.valueOf(#颜色));
	结束 方法
结束 类